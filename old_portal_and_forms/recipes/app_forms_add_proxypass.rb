node[:deploy].each do |app_name, deploy|
  if app_name != 'helpcentreuk' && app_name != 'kayako' then
    template "/etc/httpd/sites-available/#{app_name}.conf.d/local-proxypass.conf" do
      mode "0644"
      source "proxypass.erb"
      variables(:application => deploy)
      action :create_if_missing
    end
    template "/etc/httpd/sites-available/#{app_name}.conf.d/local-ssl-proxypass.conf" do
      mode "0644"
      source "proxypass-ssl.erb"
      action :create_if_missing
    end
  end
end
template "/etc/httpd/mods-available/ssl.conf" do
  mode "0644"
  source "ssl.conf.erb"
  action :create
end

template "/etc/httpd/ssl/dhparams.pem" do
  mode "0644"
  source "dhparams.pem.erb"
  action :create
end

