node[:deploy].each do |app_name, deploy|
  if app_name == 'helpcentreuk'
    directory "#{deploy[:deploy_to]}/current/templates_c" do
      mode "0777"
    end
  end
end
