node[:deploy].each do |app_name, deploy|
  if app_name == 'kayako'
    link "#{deploy[:deploy_to]}/current/__swift/files" do
      to "/mnt/kayako-attachments"
    end
  end
end