yum_package "php-imap"

template "/etc/php.ini" do
  mode "0644"
  source "php.ini.erb"
  action :create
end
